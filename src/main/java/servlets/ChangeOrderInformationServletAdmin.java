package servlets;

import tables.Orders;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Servlet prepare order for changes
 * Created by pavelpetrov on 14.10.16.
 */

public class ChangeOrderInformationServletAdmin extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String orderNumber = request.getParameter("orderButton");
        int orderNumberInt = Integer.parseInt(orderNumber);
        List<Orders> ordersList = (List<Orders>) request.getSession().getAttribute("OrderListAdmin");
        Orders orderToChange = ordersList.get(orderNumberInt);
        request.getSession().setAttribute("orderToChange", orderToChange);
        response.sendRedirect("/HTMLCSS/OrderChangeInfromationAdmin.jsp");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}

package servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * LogOut servlet log out user from the system
 * Created by pavelpetrov on 15.10.16.
 */

public class LogOutServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getSession().setAttribute("username", null);
        request.getSession().setAttribute("orderListByClientName", null);
        request.getSession().setAttribute("client", null);
        request.getSession().setAttribute("ProductsOnCardQuantity", null);
        request.getSession().setAttribute("ListOfProductOnCard", null);
        request.getSession().setAttribute("TotalPrice", null);
        response.sendRedirect("/ProductListServlet");
    }
}

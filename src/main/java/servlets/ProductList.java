package servlets;

import factory.Factory;
import service.CategoryService;
import service.ProductListService;
import tables.Category;
import tables.Params;
import tables.Product;
import tables.TablesNames;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Servlet load list of Products and fill attribute with list
 * Created by pavelpetrov on 08.10.16.
 */

public class ProductList extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    /**
     * DoGet method of servlet. Load List of Products and make attribute with this list
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ProductListService productListService = new ProductListService();
        CategoryService categoryService = new CategoryService();
        List<Product> productList = productListService.getAllProducts();
        List<String> brandList = productListService.getUniqueBrandList();
        List<Category> categoryList = categoryService.getAllCategories();
        List<String> colorList = productListService.getUniqueColorList();
        List<Integer> powerList = productListService.getUniquePowerList();
        List<Integer> weightList = productListService.getUniqueWeightList();

        HttpSession session = request.getSession();
        session.setAttribute("UniqueWeightList", weightList);
        session.setAttribute("UniquePowerList", powerList);
        session.setAttribute("UniqueColorList", colorList);
        session.setAttribute("UniqueCategoryList", categoryList);
        session.setAttribute("UniqueBrandList", brandList);
        session.setAttribute("ProductList", productList);
        response.sendRedirect("/HTMLCSS/ProductList.jsp");
    }
}

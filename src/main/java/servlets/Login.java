package servlets;

import Models.HttpResult;
import com.google.gson.Gson;
import service.ClientService;
import service.OrderSelectService;
import tables.Client;
import tables.Orders;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;


/**
 * servlet realised login structure
 * Created by pavelpetrov on 02.10.16.
 */

public class Login extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpResult result = new HttpResult();
        response.setContentType("application/json;charset=utf-8");
        Gson gson = new Gson();
        PrintWriter pw = response.getWriter();
        String username = (String) request.getParameter("username");
        String password = (String) request.getParameter("password");
        HttpSession session = request.getSession();
        ClientService clientService = new ClientService(username, password);
        if (clientService.loginClient()) {
            session.setAttribute("username", username);
            Client client = clientService.getClient(username);
            OrderSelectService orderSelectServlet = new OrderSelectService();
            List<Orders> ordersListByName = orderSelectServlet.getListOfOrderByClient(username);
            session.setAttribute("orderListByClientName", ordersListByName);
            session.setAttribute("client", client);
            result.setData("login and password are correct");
        } else {
            result.setMessage("Login or password is incorrect");
        }
        response.getWriter().write(gson.toJson(result));
        response.getWriter().flush();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/Registration.jsp").forward(request, response);
    }
}

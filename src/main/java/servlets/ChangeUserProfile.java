package servlets;

import service.ClientService;
import tables.Client;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * servlet update client information in DB
 * Created by pavelpetrov on 15.10.16.
 */
public class ChangeUserProfile extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ClientService clientService = new ClientService();
        String firstName = request.getParameter("firstName");
        String secondName = request.getParameter("secondName");
        String clientAddress = request.getParameter("clientAddress");
        Client client = (Client) request.getSession().getAttribute("client");
        client.setFirstName(firstName);
        client.setSecondName(secondName);
        client.setClientAddress(clientAddress);
        clientService.updateClient(client);
        response.sendRedirect("/HTMLCSS/ProfileForm.jsp");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}

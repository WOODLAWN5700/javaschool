package servlets;

import Models.HttpResult;
import com.google.gson.Gson;
import service.SelectService;
import tables.Product;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * make selection in OrderList
 * Created by pavelpetrov on 13.10.16.
 */

public class SelectServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        SelectService selectService = new SelectService();
        String select = request.getParameter("sort");
        List<Product> productLists = selectService.getOrderedByProductByParam(select);
        request.getSession().setAttribute("ProductList", productLists);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}

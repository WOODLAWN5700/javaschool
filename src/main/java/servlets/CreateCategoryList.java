package servlets;

import service.CategoryService;
import tables.Category;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Servlet create Category list and create Session artifact
 * Created by pavelpetrov on 18.10.16.
 */

public class CreateCategoryList extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        CategoryService categoryService = new CategoryService();
        List<Category> categories = categoryService.getAllCategories();
        request.getSession().setAttribute("CategoryList", categories);
        response.sendRedirect("http://localhost:8080/HTMLCSS/ProductRegitrationAdmin.jsp");
    }
}

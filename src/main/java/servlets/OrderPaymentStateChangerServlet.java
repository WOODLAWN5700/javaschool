package servlets;

import service.OrderChangeService;
import tables.Orders;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet request Order and param of changing and through the serice update information in DB
 * Created by pavelpetrov on 14.10.16.
 */

public class OrderPaymentStateChangerServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        OrderChangeService orderChangeService = new OrderChangeService();
        String newPaymentStateOfOrder = request.getParameter("cahngePaymentState");
        Orders order = (Orders) request.getSession().getAttribute("orderToChange");
        orderChangeService.updateOrderPaymentState(order, newPaymentStateOfOrder);
        response.sendRedirect("/HTMLCSS/OrderChangeInfromationAdmin.jsp");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}

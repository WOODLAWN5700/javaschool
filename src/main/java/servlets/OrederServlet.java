package servlets;

import service.AddProductsToCardSevice;
import tables.Product;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * create OrderList, witch is ready to buy
 * Created by pavelpetrov on 10.10.16.
 */

public class OrederServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        AddProductsToCardSevice cartService = new AddProductsToCardSevice();
        String[] orderArray = request.getParameterValues("OrederCheckBox");
        List<Product> orderList = (List<Product>) request.getSession().getAttribute("ListOfProductOnCard");
        List<Product> poductsAreReadyToBuy = new ArrayList<Product>();

        for (int i = 0; i < orderArray.length; i++) {
            int numberOfProduct = Integer.parseInt(orderArray[i]);
            if (numberOfProduct > 0) {
                orderList.get(i).setQuantityOfProductsOnCart(numberOfProduct);
                poductsAreReadyToBuy.add(orderList.get(i));
            }
        }

        request.getSession().setAttribute("poductsAreReadyToBuy", poductsAreReadyToBuy);
        int quantityProductsOnMyCard = cartService.getQuantityOfProducOnCard(poductsAreReadyToBuy);
        String price = cartService.getTotalPriceOfProductsOnCart(poductsAreReadyToBuy);
        request.getSession().setAttribute("TotalPrice", price);
        request.getSession().setAttribute("ProductsOnCardQuantity", quantityProductsOnMyCard);
        response.sendRedirect("/HTMLCSS/Orders.jsp");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}

package servlets;

import service.OrderChangeService;
import tables.Orders;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Servlet changes order status and update DB information
 * Created by pavelpetrov on 14.10.16.
 */
public class OrderStatusChangerServlet extends HttpServlet {


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        OrderChangeService orderChangeService = new OrderChangeService();
        String newOrderStatus = request.getParameter("changeOrder");
        Orders order = (Orders) request.getSession().getAttribute("orderToChange");
        orderChangeService.updateOrderState(order, newOrderStatus);
        response.sendRedirect("/HTMLCSS/OrderChangeInfromationAdmin.jsp");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}

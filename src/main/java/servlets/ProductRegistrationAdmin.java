package servlets;

import Models.HttpResult;
import com.google.gson.Gson;
import service.CategoryService;
import service.ProductRegistrationService;
import tables.Category;
import tables.Params;
import tables.Product;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * product registration servlet
 * Created by pavelpetrov on 07.10.16.
 */
public class ProductRegistrationAdmin extends HttpServlet {

    /**
     * doPost method in servlet, checks income parameters with Service class and write pobjects down to DB
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpResult resultHttp = new HttpResult();
        HttpResult productHttp = null;
        HttpResult paramHttp = null;
        CategoryService categoryService = new CategoryService();
        response.setContentType("application/json;charset=utf-8");
        Gson gson = new Gson();
        ProductRegistrationService service = new ProductRegistrationService();
        String name = request.getParameter("productName");
        String category = request.getParameter("productCategory");
        String quantity = request.getParameter("quantity");
        String price = request.getParameter("price");
        String brand = request.getParameter("brand");
        String power = request.getParameter("power");
        String color = request.getParameter("color");
        String weight = request.getParameter("weight");
        String description = request.getParameter("description");
        Category category1 = categoryService.getCategoryByName(category);
        productHttp = service.createProduct(name, quantity, price, description);
        paramHttp = service.createParams(brand, power, color, weight);
        if (productHttp.getMessage().length() > 1 || paramHttp.getMessage().length() > 1) {
            if (productHttp.getMessage().equals(paramHttp.getMessage())) {
                resultHttp.setMessage(productHttp.getMessage());
            } else {
                resultHttp.setMessage(productHttp.getMessage() + "\n" + paramHttp.getMessage());
            }
        } else {
            service.addProductandParamsToDB((Product) productHttp.getData(), (Params) paramHttp.getData(), category1);
            resultHttp.setData("Product have been inserted to DB");
        }
        response.getWriter().write(gson.toJson(resultHttp));
        response.getWriter().flush();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}

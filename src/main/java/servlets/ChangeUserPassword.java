package servlets;

import Models.HttpResult;
import com.google.gson.Gson;
import service.ClientService;
import tables.Client;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.StringReader;

import static java.lang.System.out;

/**
 * servlet request params for changing user password and write to the DB
 * Created by pavelpetrov on 16.10.16.
 */

public class ChangeUserPassword extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpResult result = new HttpResult();
        response.setContentType("application/json;charset=utf-8");
        Gson gson = new Gson();
        String oldPas = request.getParameter("oldPas");
        String newPas = request.getParameter("newPas");
        String newPasAgain = request.getParameter("newPasAgain");
        Client client = (Client) request.getSession().getAttribute("client");

        if (!client.getPassword().equals(oldPas)) {
            result.setMessage("Wrong old Password!");
        } else {
            if (!newPas.equals(newPasAgain)) {
                result.setMessage("Repeated password is Wrong");
            } else {
                client.setPassword(newPas);
                ClientService clientService = new ClientService();
                clientService.updateClient(client);
                result.setData("password changed");
            }
        }

        response.getWriter().write(gson.toJson(result));
        response.getWriter().flush();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}

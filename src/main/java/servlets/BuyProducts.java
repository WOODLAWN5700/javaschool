package servlets;

import Models.HttpResult;
import com.google.gson.Gson;
import service.*;
import tables.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * servlet take all infromation than write to DB
 * Created by pavelpetrov on 11.10.16.
 */

public class BuyProducts extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpResult resultHttp = new HttpResult();
        response.setContentType("application/json;charset=utf-8");
        Gson gson = new Gson();
        DeliveryMethodService deliveryMethodService = new DeliveryMethodService();
        ClientService clientService = new ClientService();
        PaymentStateService paymentStateService = new PaymentStateService();
        OrderStateService orderStateService = new OrderStateService();
        PaymentMethodService paymentMethodService = new PaymentMethodService();

        String delivery = request.getParameter("delivery");
        String payment = request.getParameter("payment");
        String clientLogin = (String) request.getSession().getAttribute("username");
        String comments = request.getParameter("description");

        Client client = clientService.getClient(clientLogin);
        DeliveryMethod deliveryMethod = deliveryMethodService.getDeliveryMethod(delivery);
        List<Product> productList = (List<Product>) request.getSession().getAttribute("poductsAreReadyToBuy");
        PaymentState paymentState = paymentStateService.getPaymentStateByMethod(payment);
        OrderState orderState = orderStateService.getOrderStateStock();
        Date date = new Date();
        PaymentMethod paymentMethod = paymentMethodService.getPaymentMethod(payment);
        OrdersService ordersService = new OrdersService(comments, deliveryMethod, date, orderState, paymentState, productList, paymentMethod, client);
        ordersService.fillTheOrderTable();
        request.getSession().setAttribute("ProductsOnCardQuantity", null);
        request.getSession().setAttribute("ListOfProductOnCard", null);
        request.getSession().setAttribute("TotalPrice", null);
        resultHttp.setData("Order Have been completed!");
        response.getWriter().write(gson.toJson(resultHttp));
        response.getWriter().flush();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}

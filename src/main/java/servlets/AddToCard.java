package servlets;

import service.AddProductsToCardSevice;
import tables.Product;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Servlet checks what exactly product added to card
 * Created by pavelpetrov on 08.10.16.
 */

public class AddToCard extends HttpServlet {


    /**
     * doPost method got Button value from ProductList page, and add exactly pointed product to Card
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Product> productListOnMyCard = null;
        AddProductsToCardSevice addProductsToCardSevice = new AddProductsToCardSevice();
        String button = request.getParameter("Button");
        int numberOfButton = Integer.parseInt(button);
        List<Product> productList = (List<Product>) request.getSession().getAttribute("ProductList");
        Product product = productList.get(numberOfButton);
        if (request.getSession().getAttribute("ListOfProductOnCard") == null) {
            productListOnMyCard = new ArrayList<Product>();
            productListOnMyCard = addProductsToCardSevice.getListOgProductsOnCart(productListOnMyCard, product);
        } else {
            productListOnMyCard = (List<Product>) request.getSession().getAttribute("ListOfProductOnCard");
            productListOnMyCard = addProductsToCardSevice.getListOgProductsOnCart(productListOnMyCard, product);
        }
        request.getSession().setAttribute("ListOfProductOnCard", productListOnMyCard);
        int quantityProductsOnMyCard = addProductsToCardSevice.getQuantityOfProducOnCard(productListOnMyCard);
        String price = addProductsToCardSevice.getTotalPriceOfProductsOnCart(productListOnMyCard);
        request.getSession().setAttribute("TotalPrice", price);
        request.getSession().setAttribute("ProductsOnCardQuantity", quantityProductsOnMyCard);
        response.sendRedirect("/ProductListServlet");

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}

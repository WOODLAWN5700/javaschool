package servlets;

import Models.HttpResult;
import com.google.gson.Gson;
import service.FilterProductService;
import tables.Product;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * servlet helps in product filtering
 * Created by pavelpetrov on 20.10.16.
 */

public class FilterProductServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpResult resultHttp = new HttpResult();
        response.setContentType("application/json;charset=utf-8");
        Gson gson = new Gson();
        FilterProductService filterProductService = new FilterProductService();
        String category = request.getParameter("categoryFilter");
        String color = request.getParameter("colorFilter");
        String brand = request.getParameter("brandFilter");
        String power = request.getParameter("powerFilter");
        String weight = request.getParameter("weightFilter");
        String minPrice = request.getParameter("priceMinFilter");
        String maxPrice = request.getParameter("priceMaxFilter");
        List<Product> productList = filterProductService.
                productList(category, color, power, weight, minPrice, maxPrice, brand);
        request.getSession().setAttribute("ProductList", productList);
        resultHttp.setData("Filtered ProductList have been reload ");
        response.getWriter().write(gson.toJson(resultHttp));
        response.getWriter().flush();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}

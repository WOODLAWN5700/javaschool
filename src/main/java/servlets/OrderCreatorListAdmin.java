package servlets;

import factory.Factory;
import tables.Orders;
import tables.TablesNames;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Class returns an orderList to jsp page
 * Created by pavelpetrov on 13.10.16.
 */

public class OrderCreatorListAdmin extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<Orders> ordersList = (List<Orders>) Factory.getSessionFactory().getOrderDAOImpl()
                .getAllObjectsFromTable(TablesNames.ORDER);
        HttpSession session = request.getSession();
        session.setAttribute("OrderListAdmin", ordersList);
        response.sendRedirect("/HTMLCSS/OrderListAdmin.jsp");
    }
}

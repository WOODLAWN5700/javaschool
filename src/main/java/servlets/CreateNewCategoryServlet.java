package servlets;

import Models.HttpResult;
import com.google.gson.Gson;
import service.CategoryService;
import service.ClientService;
import tables.Category;
import tables.Client;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet create new Category and add to DB with CategoryService
 * Created by pavelpetrov on 18.10.16.
 */
@WebServlet(name = "CreateNewCategoryServlet")
public class CreateNewCategoryServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Category category = new Category();
        HttpResult result = null;
        CategoryService categoryService = new CategoryService();
        response.setContentType("application/json;charset=utf-8");
        Gson gson = new Gson();
        String categoryParam = request.getParameter("category");
        result = categoryService.createNewCategoryCheckParam(category, categoryParam);
        response.getWriter().write(gson.toJson(result));
        response.getWriter().flush();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}

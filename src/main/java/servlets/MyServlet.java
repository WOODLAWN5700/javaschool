package servlets;

import factory.Factory;
import tables.Client;
import tables.TablesNames;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created by pavelpetrov on 01.10.16.
 */

public class MyServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        super.doPost(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");

        PrintWriter pw = response.getWriter();
        pw.println("<B>Client list</B>");
        pw.println("<table border=5>");
        try {
            List<Client> l = Factory.getSessionFactory().getClientEntityDAO().getAllObjectsFromTable(TablesNames.CLIENT);
            for (Client gr : l) {
                pw.println("<tr>");
                pw.println("<td>" + gr.getFirstName() + "</td>");
                pw.println("<td>" + gr.getSecondName() + "</td>");
                pw.println("<td>" + gr.getLogin() + "</td>");
                pw.println("<td>" + gr.getPassword() + "</td>");
                pw.println("<td>" + gr.getClientAddress() + "</td>");
                pw.println("</tr>");
            }
        } catch (Exception e) {
            throw new ServletException(e);
        }
    }
}

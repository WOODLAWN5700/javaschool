package servlets;

import Models.HttpResult;
import com.google.gson.Gson;
import factory.Factory;
import service.ClientService;
import tables.Client;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Registration Servlet get parameters prom registration page ind with ClientService add new client to DB.
 * Created by pavelpetrov on 04.10.16.
 */
public class Registration extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Client client = new Client();
        HttpResult result = new HttpResult();
        ClientService clientService = new ClientService();
        response.setContentType("application/json;charset=utf-8");
        Gson gson = new Gson();
        String firstName = request.getParameter("firstName");
        String lastName =  request.getParameter("seconName");
        String login = request.getParameter("email");
        String password = request.getParameter("password");
        String passwordAgain = request.getParameter("password_confirm");
        String clientAddress = request.getParameter("clientAddress");
        if (firstName.isEmpty() || lastName.isEmpty() || login.isEmpty() || password.isEmpty()
              || passwordAgain.isEmpty() || clientAddress.isEmpty()) {
            result.setMessage("Cannot be any empty fields in registration form");
        } else {
            if (!password.equals(passwordAgain)) {
                result.setMessage("Repeated password is Wrong");
            } else {
                if (clientService.getClient(login) != null) {
                    result.setMessage("Client with this login is already exist! Please choose another login");
                } else {
                    client.setFirstName(firstName);
                    client.setSecondName(lastName);
                    client.setLogin(login);
                    client.setPassword(password);
                    client.setClientAddress(clientAddress);
                    clientService.addNewClientToDB(client);
                    result.setData("Registration completed");
                }
            }
        }
        response.getWriter().write(gson.toJson(result));
        response.getWriter().flush();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}

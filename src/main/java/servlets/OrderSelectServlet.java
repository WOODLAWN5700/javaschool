package servlets;

import service.OrderSelectService;
import service.SelectService;
import tables.Orders;
import tables.Product;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Servlet make selection in OrederListAdminPage
 * Created by pavelpetrov on 14.10.16.
 */

public class OrderSelectServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        OrderSelectService selectService = new OrderSelectService();
        String select = request.getParameter("orderSort");
        List<Orders> ordersList = selectService.getSortedOrdersList(select);
        request.getSession().setAttribute("OrderListAdmin", ordersList);
//        response.sendRedirect("/HTMLCSS/OrderListAdmin.jsp");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}

package Util;

import org.hibernate.SessionFactory;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Class Util.HibernateUtil created for getting SessionFactory, for future working
 * with DB through Hibernate. Class consist only one method witch returns
 * Session factory, with singleton lazy initialization
 * pavelpetrov on 27.09.16.
 */


public class HibernateUtil {

    /**
     * Private sessionFactory before initialization
     */

    private static EntityManagerFactory sessionFactory;

    /**
     * private constructor
     */
    private HibernateUtil() {

    }

    /**
     * Method creats new entityManagerFactory if it not exist and returns old session factory if it's not null
     *
     * @return sessionFactory
     */
    public static EntityManagerFactory getSessionFactory() {
        EntityManagerFactory localSessionFactory = sessionFactory;
        if (localSessionFactory == null) {
            synchronized (HibernateUtil.class) {
                localSessionFactory = sessionFactory;
                if (localSessionFactory == null) {
                    try {
                        sessionFactory = localSessionFactory = Persistence.createEntityManagerFactory("SchoolPU");
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return sessionFactory;
    }
}

package service;

import Models.HttpResult;
import factory.Factory;
import tables.Category;
import tables.Params;
import tables.Product;

/**
 * Product registration Service
 * class checks income parameters and write objects down to tables
 * Created by pavelpetrov on 07.10.16.
 */
public class ProductRegistrationService {

    /**
     * private product
     */
    private Product product;

    /**
     * pricate params
     */
    private Params params;

    /**
     * method check income param and create new Product.
     *
     * @param name        name of product
     * @param quantity    quantity of product in stock
     * @param price       price pf product
     * @param description discription of product
     * @return Product object
     */
    public HttpResult createProduct(String name, String quantity, String price, String description) {
        HttpResult httpResult = new HttpResult();
        if (!name.isEmpty() || !quantity.isEmpty() || !price.isEmpty() || !description.isEmpty()) {
            if (!checkDigids(quantity) || !checkDigids(price)) {
                if (!checkDigids(quantity)) {
                    httpResult.setMessage("Quantity may be only numbers");
                }
                if (!checkDigids(price)) {
                    if (httpResult.getMessage().length() > 1) {
                        httpResult.setMessage(httpResult.getMessage() + "\n" + "Price may be only numbers");
                    } else {
                        httpResult.setMessage("Price may be only numbers");
                    }
                }
            } else {
                int pructPrice = Integer.parseInt(price);
                int productQuantity = Integer.parseInt(quantity);
                product = new Product(description, pructPrice, name, productQuantity);
                httpResult.setData(product);
            }
        } else {
            httpResult.setMessage("Please don't leave empty fields");
        }
        return httpResult;
    }

    /**
     * method check income params and create new Params and Http hold new Param ore error message
     *
     * @param brand  brand
     * @param power  power
     * @param color  color
     * @param weight weight
     * @return HttpResult
     */
    public HttpResult createParams(String brand, String power, String color, String weight) {
        HttpResult httpResult = new HttpResult();
        if (!brand.isEmpty() || !power.isEmpty() || !color.isEmpty() || !weight.isEmpty()) {
            if (!checkDigids(power) || !checkDigids(weight)) {
                if (!checkDigids(power)) {
                    httpResult.setMessage("Power may be only numbers");
                }
                if (!checkDigids(weight)) {
                    if (httpResult.getMessage().length() > 1) {
                        httpResult.setMessage(httpResult.getMessage() + "\n" + "Weight may be only numbers");
                    } else {
                        httpResult.setMessage("Weight may be only numbers");
                    }
                }
            } else {
                int paramWeight = Integer.parseInt(weight);
                int paramPower = Integer.parseInt(power);
                params = new Params(brand, color, paramPower, paramWeight);
                httpResult.setData(params);
            }
        } else {
            httpResult.setMessage("Please don't leave empty fields");
        }
        return httpResult;
    }

    /**
     * insert Product and his params to DB
     *
     * @param product  Product that we want to add to DB
     * @param params   Param table
     * @param category CAtegory
     */
    public void addProductandParamsToDB(Product product, Params params, Category category) {
        product.setCategory(category);
        product.setParametr(params);
        Factory.getSessionFactory().getParamsDaoImpl().insertToTable(params);
        Factory.getSessionFactory().getProductDaoImpl().insertToTable(product);
    }

    /**
     * check text on digids
     *
     * @param text
     * @return boolean true if text contains only numbers
     */
    public Boolean checkDigids(String text) {
        String regex = "^(\\d+)$";
        return text.matches(regex);
    }

}

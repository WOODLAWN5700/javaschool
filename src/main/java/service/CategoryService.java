package service;

import Models.HttpResult;
import factory.Factory;
import tables.Category;
import tables.TablesNames;

import java.util.List;

/**
 * business logic with category table
 * Created by pavelpetrov on 18.10.16.
 */
public class CategoryService {
    /**
     * method returns unique Category by name
     *
     * @param categoryName name of category in table;
     * @return Category
     */
    public Category getCategoryByName(String categoryName) {
        List<Category> categories = Factory.getSessionFactory().getCategoryEntityDao().getAllObjectsFromeTableWithParamName(TablesNames.CATEGORY, "category", categoryName);
        if (categories.isEmpty()) {
            return null;
        } else {
            Category categoryByName = categories.get(0);
            return categoryByName;
        }
    }

    /**
     * insert to Table new Category in DB
     *
     * @param category
     */
    public void createNewCategory(Category category) {
        Factory.getSessionFactory().getClientEntityDAO().insertToTable(category);
    }

    /**
     * method returns httpResult, set message if something wrong, set data if everything is ok.
     *
     * @param category      new category
     * @param categoryParam category name
     * @return HttpResult
     */
    public HttpResult createNewCategoryCheckParam(Category category, String categoryParam) {
        HttpResult httpResult = new HttpResult();
        Category category1 = getCategoryByName(categoryParam);
        if (!categoryParam.isEmpty()) {
            if (category1 == null) {
                category.setCategory(categoryParam);
                createNewCategory(category);
                httpResult.setData("Category have been added!");
            } else {
                httpResult.setMessage("This category is already exist!");
            }
        } else {
            httpResult.setMessage("Sorry, but we can't add empty category!");
        }
        return httpResult;
    }

    /**
     * Method returns list of all categories from table Category
     *
     * @return list of categories
     */
    public List<Category> getAllCategories() {
        List<Category> categoryList = Factory.getSessionFactory().getCategoryEntityDao()
                .getAllObjectsFromTable(TablesNames.CATEGORY);
        return categoryList;
    }
}

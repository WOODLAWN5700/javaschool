package service;

import factory.Factory;
import tables.PaymentMethod;
import tables.TablesNames;

import java.util.List;

/**
 * class works with payment method table
 * Created by pavelpetrov on 11.10.16.
 */
public class PaymentMethodService {

    /**
     * method returns PaymentMethod by name
     * @param payment payment Method
     * @return PaymentMethod
     */
    public PaymentMethod getPaymentMethod(String payment) {
        List<PaymentMethod> paymentMethodList = Factory.getSessionFactory().getPaymentMethodImpl()
                .getAllObjectsFromeTableWithParamName(TablesNames.PAYMENT_METHOD, "paymetnMethod", payment);
        PaymentMethod paymentMethod = paymentMethodList.get(0);
        return paymentMethod;
    }
}

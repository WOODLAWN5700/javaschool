package service;

import factory.Factory;
import tables.Client;
import tables.TablesNames;

import java.util.List;

/**
 * CLass witch working eith loginServlet
 * Created by pavelpetrov on 02.10.16.
 */
public class ClientService {
    /**
     * login of client
     */
    private String login;

    /**
     * client's password
     */
    private String password;

    /**
     * empty constructor
     */
    public ClientService() {
    }

    /**
     * ClientService construct
     *
     * @param login    of client
     * @param password of client
     */
    public ClientService(String login, String password) {
        this.login = login;
        this.password = password;
    }

    /**
     * method checks client with two parameters
     *
     * @return client list
     */
    public boolean loginClient() {
        boolean clientIsInTheList = true;
        if (Factory.getSessionFactory().getClientEntityDAO().getAllObjectsFromeTableWithParamName
                (TablesNames.CLIENT, "login", "password", login, password).isEmpty()) {
            clientIsInTheList = false;
        }
        return clientIsInTheList;
    }

    /**
     * method returns client with unique login
     *
     * @param login unique login of client
     * @return client with login
     */
    public Client getClient(String login) {
        List<Client> clientList = (List<Client>) Factory.getSessionFactory().getClientEntityDAO()
                .getAllObjectsFromeTableWithParamName(TablesNames.CLIENT, "login", login);
        if (clientList.isEmpty()) {
            return null;
        } else {
            Client client = clientList.get(0);
            return client;
        }
    }

    /**
     * method update information in DB
     *
     * @param client updatable client
     */
    public void updateClient(Client client) {
        Factory.getSessionFactory().getClientEntityDAO().updateTable(client);
    }

    /**
     * method add new client to DB
     *
     * @param client new client
     */
    public void addNewClientToDB(Client client) {
        Factory.getSessionFactory().getClientEntityDAO().insertToTable(client);
    }

}

package service;

import factory.Factory;
import tables.DeliveryMethod;
import tables.TablesNames;

import java.util.List;

/**
 * class working with delivery method table
 * Created by pavelpetrov on 11.10.16.
 */
public class DeliveryMethodService {

    /**
     * method returns DeliveryMethod by param.
     * @param delivery name of delivery Method
     * @return DeliveryMethod
     */
    public DeliveryMethod getDeliveryMethod(String delivery) {
        List<DeliveryMethod> deliveryMethodList = Factory.getSessionFactory().getDeliveryMethodDaoImpl()
                .getAllObjectsFromeTableWithParamName(TablesNames.DELIVERY_METHOD, "deliveryMethod", delivery);
        DeliveryMethod deliveryMethod = deliveryMethodList.get(0);
        return deliveryMethod;
    }
}

package service;

import tables.Product;

import javax.persistence.criteria.Order;
import java.util.List;
import java.util.Objects;

/**
 * Service class realize bossiness logic of adding products to cart
 * Created by pavelpetrov on 18.10.16.
 */
public class AddProductsToCardSevice {

    /**
     * serach product in the List, if find it we plus counter else we add to List new product with count ==1
     *
     * @param productList product list
     * @param product     picked product
     * @return Product List
     */
    public List<Product> getListOgProductsOnCart(List<Product> productList, Product product) {
        int count = 0;
        for (Product pr : productList) {
            if (pr.equals(product)) {
                int quantity = pr.getQuantityOfProductsOnCart();
                Product product2 = pr;
                product2.setQuantityOfProductsOnCart(++quantity);
                productList.remove(pr);
                productList.add(product2);
                break;
            }
            count++;
        }
        if (count == productList.size()) {
            product.setQuantityOfProductsOnCart(1);
            productList.add(product);
        }
        return productList;
    }

    /**
     * method returns quantity of products on Cart
     *
     * @param products productList
     * @return quantity of products
     */
    public int getQuantityOfProducOnCard(List<Product> products) {
        int result = 0;
        for (Product p : products) {
            result += p.getQuantityOfProductsOnCart();
        }
        return result;
    }

    public String getTotalPriceOfProductsOnCart(List<Product> products) {
        int price = 0;
        for (Product p : products) {
            price += p.getPrice() * p.getQuantityOfProductsOnCart();
        }
        String totalPrice = price + " $";
        return totalPrice;
    }

}

package service;

import factory.Factory;
import tables.Product;
import tables.TablesNames;

import java.util.List;

/**
 * Select service for Select servlet
 * Created by pavelpetrov on 13.10.16.
 */
public class SelectService {

    /**
     * Method retutns Product List ordered by param
     *
     * @param param ordered param
     * @return product list
     */
    public List<Product> getOrderedByProductByParam(String param) {
        List<Product> productList = null;
        if (param.equals("brand")) {
            productList = (List<Product>) Factory.getSessionFactory().getProductDaoImpl().
                    getAllObgectsFromTableByOrderParams(TablesNames.PRODUCT, param, "parametr");
        } else {
            productList = (List<Product>) Factory.getSessionFactory().getProductDaoImpl()
                    .getAllObgectsFromTableByOrder("Product", param);
        }
        return productList;
    }
}

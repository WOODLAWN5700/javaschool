package service;

import factory.Factory;
import tables.OrderState;

/**
 *  class workin g whit Order-state table
 * Created by pavelpetrov on 11.10.16.
 */
public class OrderStateService {

    /**
     * method returns order state in the stock
     */
    public OrderState getOrderStateStock() {
        OrderState orderState = (OrderState) Factory.getSessionFactory().getOrderStateDaoImpl().getObjectFromTable(1);
        return orderState;
    }
}

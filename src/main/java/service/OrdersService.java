package service;

import factory.Factory;
import tables.*;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * class workinf with Orders table
 * Created by pavelpetrov on 11.10.16.
 */
public class OrdersService {
    private Orders orders;
    private Client client;
    private PaymentMethod paymentMethod;
    private List<Product> products;
    private PaymentState paymentState;
    private OrderState orderState;
    private Date date;
    private DeliveryMethod deliveryMethod;
    private String comments;

    /**
     * constructor of OrderService
     *
     * @param comments
     * @param deliveryMethod
     * @param date
     * @param orderState
     * @param paymentState
     * @param products
     * @param paymentMethod
     * @param client
     */
    public OrdersService(String comments, DeliveryMethod deliveryMethod, Date date, OrderState orderState,
                         PaymentState paymentState, List<Product> products, PaymentMethod paymentMethod, Client client) {
        this.comments = comments;
        this.deliveryMethod = deliveryMethod;
        this.date = date;
        this.orderState = orderState;
        this.paymentState = paymentState;
        this.products = products;
        this.paymentMethod = paymentMethod;
        this.client = client;
    }

    public void fillTheOrderTable() {
        orders = new Orders();
        orders.setClient(client);
        orders.setComments(comments);
        orders.setDateOfOrder(date);
        orders.setDeliveryMethod(deliveryMethod);
        orders.setOrderState(orderState);
        orders.setPaymentMethod(paymentMethod);
        Set<Product> poductSet = new HashSet<>();
        for (Product p : products) {
            poductSet.add(p);
        }
        orders.setProducts(poductSet);
        orders.setPaymentState(paymentState);
        Factory.getSessionFactory().getOrderDAOImpl().insertToTable(orders);
    }
}

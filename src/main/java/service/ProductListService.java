package service;

import factory.Factory;
import tables.Product;
import tables.TablesNames;

import java.util.List;

/**
 * Class working with ProductList Page information for sort and filter Product List
 * Created by pavelpetrov on 19.10.16.
 */
public class ProductListService {

    /**
     * method returns product List
     *
     * @return product list
     */
    public List<Product> getAllProducts() {
        List<Product> products = (List<Product>) Factory.getSessionFactory().getProductDaoImpl().getAllObjectsFromTable(TablesNames.PRODUCT);
        return products;
    }

    /**
     * method returns unique brand List
     *
     * @return brand List
     */
    public List<String> getUniqueBrandList() {
        List<String> uniqueBrandList = Factory.getSessionFactory().getProductDaoImpl().
                getUniqueListObjectFromJoinAnotherTable(TablesNames.PRODUCT, "parametr", "brand");
        return uniqueBrandList;
    }

    /**
     * method returns unique color List
     *
     * @return color List
     */
    public List<String> getUniqueColorList() {
        List<String> uniqueColorList = Factory.getSessionFactory().getProductDaoImpl().
                getUniqueListObjectFromJoinAnotherTable(TablesNames.PRODUCT, "parametr", "color");
        return uniqueColorList;
    }

    /**
     * method returns unique power List
     *
     * @return power List
     */
    public List<Integer> getUniquePowerList() {
        List<Integer> uniquePowerList = Factory.getSessionFactory().getProductDaoImpl().
                getUniqueListObjectFromJoinAnotherTableForInteger(TablesNames.PRODUCT, "parametr", "power");
        return uniquePowerList;
    }

    /**
     * method returns unique weight List
     *
     * @return weight List
     */
    public List<Integer> getUniqueWeightList() {
        List<Integer> uniqueWeightList = Factory.getSessionFactory().getProductDaoImpl().
                getUniqueListObjectFromJoinAnotherTableForInteger(TablesNames.PRODUCT, "parametr", "weigth");
        return uniqueWeightList;
    }


}

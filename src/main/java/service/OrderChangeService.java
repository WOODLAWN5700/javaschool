package service;

import factory.Factory;
import tables.OrderState;
import tables.Orders;
import tables.PaymentState;
import tables.TablesNames;

import java.util.List;

/**
 * class works with OrderState and update information in DB
 * Created by pavelpetrov on 14.10.16.
 */
public class OrderChangeService {

    /**
     * method change OrderState in Orders table and update information
     * @param order Order
     * @param orderState param Order State
     */
    public void updateOrderState(Orders order, String orderState) {
        List<OrderState> orderState1 = Factory.getSessionFactory().getOrderStateDaoImpl()
                .getAllObjectsFromeTableWithParamName(TablesNames.ORDER_STATE, "orderState", orderState);
        order.setOrderState(orderState1.get(0));
        Factory.getSessionFactory().getOrderDAOImpl().updateTable(order);
    }

    /**
     * method change PaymentState in Orders table and update information in Db
     * @param order Order
     * @param paymentStateParam param Paymnet state of order
     */
    public void updateOrderPaymentState(Orders order, String paymentStateParam) {
        List<PaymentState> paymentState = Factory.getSessionFactory().getPaymentStateDaoImpl()
                .getAllObjectsFromeTableWithParamName(TablesNames.PAYMENT_STATE, "paymentState", paymentStateParam);
        order.setPaymentState(paymentState.get(0));
        Factory.getSessionFactory().getOrderDAOImpl().updateTable(order);
    }
}

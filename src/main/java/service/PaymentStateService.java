package service;

import factory.Factory;
import tables.PaymentState;

/**
 * class workin g whit payment_service table
 * Created by pavelpetrov on 11.10.16.
 */
public class PaymentStateService {

    /**
     * method returns PaymentState depend of payment method
     *
     * @param payment payment method
     * @return PaymentState
     */
    public PaymentState getPaymentStateByMethod(String payment) {
        PaymentState paymentState = null;
        if (payment.equals("cash")) {
            paymentState = (PaymentState) Factory.getSessionFactory().getPaymentStateDaoImpl().getObjectFromTable(2);
        } else {
            paymentState = (PaymentState) Factory.getSessionFactory().getPaymentStateDaoImpl().getObjectFromTable(1);
        }
        return paymentState;
    }
}

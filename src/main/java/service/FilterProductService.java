package service;

import factory.Factory;
import tables.Product;

import java.util.List;

/**
 * Service class get params and make filter of products
 * Created by pavelpetrov on 20.10.16.
 */
public class FilterProductService {
    /**
     * min price
     */
    private String minPrice;

    /**
     * max price
     */
    private String maxPrice;

    /**
     * method sort price by value
     *
     * @param firstPrice  income param of price
     * @param secondPrice income param of price
     */
    private void getMinPrice(String firstPrice, String secondPrice) {
        int price1 = Integer.parseInt(firstPrice);
        int price2 = Integer.parseInt(secondPrice);
        if (price1 < price2) {
            minPrice = firstPrice;
            maxPrice = secondPrice;
        } else {
            minPrice = secondPrice;
            maxPrice = firstPrice;
        }
    }

    /**
     * method returns filtred List of products
     *
     * @param category     category
     * @param color        color
     * @param power        power
     * @param weight       weight
     * @param minimumPrice minimumPrice
     * @param maximumPrice maximumPrice
     * @param brand        brand
     * @return list of products
     */
    public List<Product> productList(String category, String color, String power,
                                     String weight, String minimumPrice, String maximumPrice, String brand) {
        getMinPrice(minimumPrice, maximumPrice);
        List<Product> productList = Factory.getSessionFactory().getProductDaoImpl().
                getFiltredProductList(category, power, minPrice, maxPrice, brand, color, weight);
        return productList;
    }

}

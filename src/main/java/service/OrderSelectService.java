package service;

import factory.Factory;
import tables.Orders;
import tables.TablesNames;

import java.util.List;

/**
 * Class working with OrderSelcetServlet, request information from servlet and response Order sorted list
 * Created by pavelpetrov on 14.10.16.
 */
public class OrderSelectService {


    /**
     * method return sorted order List
     *
     * @param param parametr of searching
     * @return Order list
     */
    public List<Orders> getSortedOrdersList(String param) {
        List<Orders> ordersList = null;
        if (param.equals("dateOfOrder")) {
            ordersList = Factory.getSessionFactory().getOrderDAOImpl()
                    .getAllObgectsFromTableByOrder(TablesNames.ORDER, param);
        } else if (param.equals("orderState")) {
            ordersList = Factory.getSessionFactory().getOrderDAOImpl().
                    getAllObgectsFromTableByOrderParams(TablesNames.ORDER, param, "orderState");
        } else if (param.equals("login")) {
            ordersList = Factory.getSessionFactory().getOrderDAOImpl().
                    getAllObgectsFromTableByOrderParams(TablesNames.ORDER, param, "client");
        } else if (param.equals("deliveryMethod")) {
            ordersList = Factory.getSessionFactory().getOrderDAOImpl().
                    getAllObgectsFromTableByOrderParams(TablesNames.ORDER, param, "deliveryMethod");
        } else {
            ordersList = Factory.getSessionFactory().getOrderDAOImpl()
                    .getAllObgectsFromTableWhereOneOfTheColumnEqalsParam(TablesNames.ORDER, "orderState", param);
        }
        return ordersList;
    }

    /**
     * method return list of Orders by search user login
     *
     * @param userlogin String search param
     * @return List of Orders
     */
    public List<Orders> getListOfOrderByClient(String userlogin) {
        List<Orders> orderListByClientName = Factory.getSessionFactory().getOrderDAOImpl()
                .getAllObgectsFromTableWhereOneOfTheColumnDontEqalsParam(TablesNames.ORDER, "client", userlogin, "login");
        if (orderListByClientName.size() == 0) {
            return null;
        } else {
            return orderListByClientName;
        }
    }

}

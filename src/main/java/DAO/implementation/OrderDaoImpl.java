package DAO.implementation;

import tables.Orders;

/**
 * Class ClientDaoImpl consist methods of working with order table in project DB
 * Created by pavelpetrov on 27.09.16.
 */
public class OrderDaoImpl extends ObjectDaoImpl<Orders> {

    public OrderDaoImpl(Class<Orders> type) {
        super(type);
    }
}

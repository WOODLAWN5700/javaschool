package DAO.implementation;

/**
 * consist methods of working with order table in project DB
 * Created by pavelpetrov on 28.09.16.
 */
public class ProductParamDaoImpl extends ObjectDaoImpl<ProductParamDaoImpl> {
    public ProductParamDaoImpl(Class<ProductParamDaoImpl> type) {
        super(type);
    }
}

package DAO.implementation;

import tables.DeliveryMethod;

/**
 *  consist methods of working with order table in project DB
 * Created by pavelpetrov on 28.09.16.
 */
public class DeliveryMerhodDaoImpl extends ObjectDaoImpl<DeliveryMethod> {
    public DeliveryMerhodDaoImpl(Class<DeliveryMethod> type) {
        super(type);
    }
}

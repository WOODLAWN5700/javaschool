package DAO.implementation;

import tables.OrderState;

/**
 * Created by pavelpetrov on 28.09.16.
 */
public class OrderStateDaoImpl extends ObjectDaoImpl<OrderState> {
    public OrderStateDaoImpl(Class<OrderState> type) {
        super(type);
    }
}

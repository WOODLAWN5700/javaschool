package DAO.implementation;

import tables.Params;

/**
 *  consist methods of working with order table in project DB
 * Created by pavelpetrov on 28.09.16.
 */
public class ParamsDaoImpl extends ObjectDaoImpl<Params> {
    public ParamsDaoImpl(Class<Params> type) {
        super(type);
    }
}

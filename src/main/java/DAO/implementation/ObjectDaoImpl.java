package DAO.implementation;

import Util.HibernateUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;

/**
 * Abstract class implements TableEntityDao
 * with generic verable <T>
 * Created by pavelpetrov on 27.09.16.
 */
public abstract class ObjectDaoImpl<T> {
    private Class<T> type = null;

    /**
     * constructor of this class
     *
     * @param type
     */
    public ObjectDaoImpl(Class<T> type) {
        this.type = type;
    }

    /**
     * get generic.class
     *
     * @return type
     */
    public Class<T> getMyType() {
        return this.type;
    }

    /**
     * method insert object into table
     *
     * @param objectType type of table
     */
    public void insertToTable(T objectType) {
        EntityManager entityManager = HibernateUtil.getSessionFactory().createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            entityManager.persist(objectType);
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            entityManager.close();
        }
    }

    /**
     * method update object into table
     *
     * @param objectType type of table
     */

    public void updateTable(T objectType) {
        EntityManager entityManager = HibernateUtil.getSessionFactory().createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            entityManager.merge(objectType);
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            entityManager.close();
        }
    }

    /**
     * Method delete object from table
     *
     * @param objectType type og table
     */
    public void deleteFromTable(T objectType) {
        EntityManager entityManager = HibernateUtil.getSessionFactory().createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            entityManager.remove(entityManager.merge(objectType));
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            entityManager.close();
        }
    }

    /**
     * Method delelts object from table by his id
     *
     * @param id of object in the table
     */
    public void deleteFromTableById(int id) {
        T object = null;
        EntityManager entityManager = HibernateUtil.getSessionFactory().createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            object = (T) entityManager.find(getMyType(), id);
            entityManager.remove(entityManager.merge(object));
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            entityManager.close();
        }
    }


    /**
     * Metshod returns object from table by his id
     *
     * @param id object's id in table
     * @return object from table
     */
    public T getObjectFromTable(int id) {
        T object = null;
        EntityManager entityManager = HibernateUtil.getSessionFactory().createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            object = (T) entityManager.find(getMyType(), id);
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            entityManager.close();
        }
        return object;
    }

    /**
     * Method returns List of all objects wutch are in table
     *
     * @return List of objects
     */
    public List<T> getAllObjectsFromTable(String tableName) {
        List<T> objectList = null;
        EntityManager entityManager = HibernateUtil.getSessionFactory().createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            String query = "SELECT c FROM " + tableName + " c";
            transaction.begin();
            objectList = entityManager.createQuery(query, getMyType()).getResultList();
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            entityManager.close();
        }
        return objectList;
    }

    /**
     * method gives a list of objects ordered by param.
     *
     * @param tableName      name of the table in class TableName
     * @param orderedByParam sorted param
     * @return listof Objects
     */
    public List<T> getAllObgectsFromTableByOrder(String tableName, String orderedByParam) {
        List<T> objectList = null;
        EntityManager entityManager = HibernateUtil.getSessionFactory().createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            String query = "SELECT c FROM " + tableName + " c ORDER BY c." + orderedByParam;
            objectList = entityManager.createQuery(query, getMyType()).getResultList();
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            entityManager.close();
        }
        return objectList;
    }

    /**
     * method return object list ordered by param from join table
     *
     * @param tableName      name of the table from TableName class
     * @param orderedByParam sort param
     * @param joingTable     join table
     * @return object list
     */
    public List<T> getAllObgectsFromTableByOrderParams(String tableName, String orderedByParam, String joingTable) {
        List<T> objectList = null;
        EntityManager entityManager = HibernateUtil.getSessionFactory().createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            String query = "SELECT c FROM " + tableName + " c JOIN c." + joingTable +
                    " ORDER BY  c." + joingTable + "." + orderedByParam;
            objectList = entityManager.createQuery(query, getMyType()).getResultList();
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            entityManager.close();
        }
        return objectList;
    }

    /**
     * method return list of objects where one of the params equals serchParam
     *
     * @param tableName   table name from TableName class
     * @param joingTable  jion table
     * @param searchParam searchParam
     * @return objectList
     */
    public List<T> getAllObgectsFromTableWhereOneOfTheColumnEqalsParam(String tableName, String joingTable, String searchParam) {
        List<T> objectList = null;
        EntityManager entityManager = HibernateUtil.getSessionFactory().createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            String query = "SELECT c FROM " + tableName + " c JOIN c." + joingTable
                    + " o where c." + joingTable + "." + joingTable + " = '" + searchParam + "'";
            objectList = entityManager.createQuery(query, getMyType()).getResultList();
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            entityManager.close();
        }
        return objectList;
    }


    public List<T> getAllObgectsFromTableWhereOneOfTheColumnDontEqalsParam(String tableName, String joingTable, String searchParam, String serachingColumn) {
        List<T> objectList = null;
        EntityManager entityManager = HibernateUtil.getSessionFactory().createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            String query = "SELECT c FROM " + tableName + " c JOIN c." + joingTable
                    + " o where c." + joingTable + "." + serachingColumn + " = '" + searchParam + "'";
            objectList = entityManager.createQuery(query, getMyType()).getResultList();
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            entityManager.close();
        }
        return objectList;
    }

    /**
     * serching list of object by parametrs
     *
     * @param tableName       in DT
     * @param fieldName       locumn in table
     * @param firstParamValue searching value
     * @return object list
     */
    public List<T> getAllObjectsFromeTableWithParamName(String tableName, String fieldName, String firstParamValue) {
        List<T> objectList = null;
        EntityManager entityManager = HibernateUtil.getSessionFactory().createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            String query = "SELECT c FROM " + tableName + " c" + " WHERE " + "c." + fieldName + " = ?1";
            transaction.begin();
            objectList = entityManager.createQuery(query, getMyType()).setParameter(1, firstParamValue).getResultList();
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            entityManager.close();
        }
        return objectList;
    }

    /**
     * serching list of object by parametrs
     *
     * @param tableName        in DB
     * @param fieldName        first searchin field
     * @param secondField      second searching field
     * @param firstParamValue  first searchng param
     * @param secondParamValue second searching param
     * @return object list
     */
    public List<T> getAllObjectsFromeTableWithParamName(String tableName, String fieldName, String secondField,
                                                        String firstParamValue, String secondParamValue) {
        List<T> objectList = null;
        EntityManager entityManager = HibernateUtil.getSessionFactory().createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            String query = "SELECT c FROM " + tableName + " c" + " WHERE " + "c." + fieldName + " = ?1 and c."
                    + secondField + " = ?2";
            transaction.begin();
            objectList = entityManager.createQuery(query, getMyType()).setParameter(1, firstParamValue)
                    .setParameter(2, secondParamValue).getResultList();
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            entityManager.close();
        }
        return objectList;
    }

    /**
     * method for filter for Product table
     *
     * @param category           category
     * @param power              power
     * @param priceLowThreshold  priceLowThreshold
     * @param priceHighThreshold priceHighThreshold
     * @param brand              brand of product
     * @param color              color
     * @param weight             weight
     * @return filtred List
     */
    public List<T> getFiltredProductList(String category, String power,
                                         String priceLowThreshold, String priceHighThreshold, String brand, String color, String weight) {
        List<T> objectList = null;
        EntityManager entityManager = HibernateUtil.getSessionFactory().createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            String query = "SELECT c from Product c JOIN c.category JOIN c.parametr o where 1 = 1";
            if (category.length() > 0) {
                query += " AND c.category.category = '" + category + "'";
            }
            if (power.length() > 0) {
                query += " AND c.parametr.power = '" + power + "'";
            }
            if (priceLowThreshold.length() > 0) {
                query += " AND c.price >= '" + priceLowThreshold + "'";
            }
            if (priceHighThreshold.length() > 0) {
                query += " AND c.price <= '" + priceHighThreshold + "'";
            }
            if (brand.length() > 0) {
                query += " AND c.parametr.brand = '" + brand + "'";
            }
            if (color.length() > 0) {
                query += " AND c.parametr.color = '" + color + "'";
            }
            if (weight.length() > 0) {
                query += " AND c.parametr.weight = '" + weight + "'";
            }


            objectList = entityManager.createQuery(query, getMyType()).getResultList();
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            entityManager.close();
        }
        return objectList;
    }

    /**
     * method returns list of String unique columns
     * @param tableName table name
     * @param nameOfSecondTableInFirstTableEntity join table name
     * @param param column name
     * @return String list
     */
    public List<String> getUniqueListObjectFromJoinAnotherTable(String tableName,
                                                                String nameOfSecondTableInFirstTableEntity, String param) {
        List<String> objectList = null;
        EntityManager entityManager = HibernateUtil.getSessionFactory().createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            // Query query2 = entityManager.createQuery("select DISTINCT c.parametr.brand from Product c order by c.parametr.brand")
            String query = "select DISTINCT c." + nameOfSecondTableInFirstTableEntity + "." + param + " from " + tableName + " c";
            transaction.begin();
            objectList = entityManager.createQuery(query, String.class).getResultList();
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            entityManager.close();
        }
        return objectList;
    }

    /**
     * method returns list of Integer unique columns
     * @param tableName table name
     * @param nameOfSecondTableInFirstTableEntity join table name
     * @param param column name
     * @return String list
     */
    public List<Integer> getUniqueListObjectFromJoinAnotherTableForInteger(String tableName,
                                                                String nameOfSecondTableInFirstTableEntity, String param) {
        List<Integer> objectList = null;
        EntityManager entityManager = HibernateUtil.getSessionFactory().createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            // Query query2 = entityManager.createQuery("select DISTINCT c.parametr.brand from Product c order by c.parametr.brand")
            String query = "select DISTINCT c." + nameOfSecondTableInFirstTableEntity + "." + param + " from " + tableName + " c";
            transaction.begin();
            objectList = entityManager.createQuery(query, Integer.class).getResultList();
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            entityManager.close();
        }
        return objectList;
    }

}

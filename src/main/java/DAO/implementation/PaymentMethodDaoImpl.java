package DAO.implementation;

import tables.PaymentMethod;

/**
 * consist methods of working with order table in project DB
 * Created by pavelpetrov on 28.09.16.
 */
public class PaymentMethodDaoImpl extends ObjectDaoImpl<PaymentMethod> {
    public PaymentMethodDaoImpl(Class<PaymentMethod> type) {
        super(type);
    }
}

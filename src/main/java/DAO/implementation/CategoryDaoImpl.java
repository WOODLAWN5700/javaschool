package DAO.implementation;

import tables.Category;

/**
 *   consist methods of working with order table in project DB
 * Created by pavelpetrov on 28.09.16.
 */
public class CategoryDaoImpl extends ObjectDaoImpl<Category> {
    public CategoryDaoImpl(Class<Category> type) {
        super(type);
    }
}

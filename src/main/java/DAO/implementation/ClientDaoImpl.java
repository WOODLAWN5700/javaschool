package DAO.implementation;


import tables.Client;



/**
 *  consist methods of working with order table in project DB
 * Created by pavelpetrov on 27.09.16.
 */
public class ClientDaoImpl extends ObjectDaoImpl<Client> {
    public ClientDaoImpl(Class<Client> type) {
        super(type);
    }
}

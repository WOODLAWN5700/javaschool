package DAO.implementation;

import tables.PaymentState;

/**
 * consist methods of working with order table in project DB
 * Created by pavelpetrov on 28.09.16.
 */
public class PaymentStateDaoImpl extends ObjectDaoImpl<PaymentState> {
    public PaymentStateDaoImpl(Class<PaymentState> type) {
        super(type);
    }
}

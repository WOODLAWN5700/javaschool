package DAO.implementation;

import tables.Product;

/**
 * consist methods of working with order table in project DB
 * Created by pavelpetrov on 28.09.16.
 */
public class ProductDaoImpl extends ObjectDaoImpl<Product> {
    public ProductDaoImpl(Class<Product> type) {
        super(type);
    }
}

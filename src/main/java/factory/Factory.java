package factory;


import DAO.implementation.*;
import tables.*;

/**
 * Factory class returns table DAO
 * Created by pavelpetrov on 27.09.16.
 */
public class Factory {

    /**
     * Private factory before initialization
     */
    private static Factory instanse;

    /**
     * Private tableEntityDAO before initialization
     */
    private ObjectDaoImpl tableEntityDAO = null;


    /**
     * private constructor
     */
    private Factory() {
    }

    /**
     * Method creats new Factory if it not exist and returns old session factory if it's not null.
     * lazy synchronized initialization
     *
     * @return instanse (Factory)
     */
    public static Factory getSessionFactory() {
        Factory licalInstanse = instanse;
        if (licalInstanse == null) {
            synchronized (Factory.class) {
                licalInstanse = instanse;
                if (licalInstanse == null) {
                    try {
                        instanse = licalInstanse = new Factory();
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return instanse;
    }

    /**
     * Method initializes tableEntityDAO to ClientDaoImpl
     *
     * @return tableEntityDAO
     */
    public ObjectDaoImpl getClientEntityDAO() {
        tableEntityDAO = new ClientDaoImpl(Client.class);
        return tableEntityDAO;
    }

    /**
     * Method initializes tableEntityDAO to OrderDaoImpl
     *
     * @return tableEntityDAO
     */
    public ObjectDaoImpl getOrderDAOImpl() {
        tableEntityDAO = new OrderDaoImpl(Orders.class);
        return tableEntityDAO;
    }


    /**
     * Method initializes tableEntityDAO to categoryDaoImpl
     *
     * @return tableEntityDAO
     */
    public ObjectDaoImpl getCategoryEntityDao() {
        tableEntityDAO = new CategoryDaoImpl(Category.class);
        return tableEntityDAO;
    }

    /**
     * Method initializes tableEntityDAO to categoryDaoImpl
     *
     * @return tableEntityDAO
     */
    public ObjectDaoImpl getDeliveryMethodDaoImpl() {
        tableEntityDAO = new DeliveryMerhodDaoImpl(DeliveryMethod.class);
        return tableEntityDAO;
    }

    /**
     * Method initializes tableEntityDAO to orderStateDaoImpl
     *
     * @return tableEntityDAO
     */
    public ObjectDaoImpl getOrderStateDaoImpl() {
        tableEntityDAO = new OrderStateDaoImpl(OrderState.class);
        return tableEntityDAO;
    }


    /**
     * Method initializes tableEntityDAO to paramsDaoImpl
     *
     * @return tableEntityDAO
     */
    public ObjectDaoImpl getParamsDaoImpl() {
        tableEntityDAO = new ParamsDaoImpl(Params.class);
        return tableEntityDAO;
    }

    /**
     * Method initializes tableEntityDAO to paymentMethodIml
     *
     * @return tableEntityDAO
     */
    public ObjectDaoImpl getPaymentMethodImpl() {
        tableEntityDAO = new PaymentMethodDaoImpl(PaymentMethod.class);
        return tableEntityDAO;
    }

    /**
     * Method initializes tableEntityDAO to paymentStateDaoImpl
     *
     * @return tableEntityDAO
     */
    public ObjectDaoImpl getPaymentStateDaoImpl() {
        tableEntityDAO = new PaymentStateDaoImpl(PaymentState.class);
        return tableEntityDAO;
    }

    /**
     * Method initializes tableEntityDAO to productDaoImpl
     *
     * @return tableEntityDAO
     */
    public ObjectDaoImpl getProductDaoImpl() {
        tableEntityDAO = new ProductDaoImpl(Product.class);
        return tableEntityDAO;
    }
}

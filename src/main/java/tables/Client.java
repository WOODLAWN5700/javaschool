package tables;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Hiberanate class discolate entity of "Client" table in DB
 * Created by pavelpetrov on 27.09.16.
 */

@Entity
@Table(name = "client")
@AttributeOverride(name = "entityId", column = @Column(name = "client_id"))
public class Client extends TableEntity {

    /**
     * first name of client
     */
    @Column(name = "first_name")
    private String firstName;

    /**
     * second name of client
     */
    @Column(name = "cecond_name")
    private String secondName;

    /**
     * client's address
     */
    @Column(name = "client_address")
    private String clientAddress;

    /**
     * client's login/ his eMail
     */
    @Column(name = "login", unique = true)
    private String login;

    /**
     * client's passwod
     */
    @Column(name = "password")
    private String password;

    /**
     * set of orderses
     */
    @OneToMany(mappedBy = "client")
    private Set<Orders> orderses = new HashSet<Orders>();


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getClientAddress() {
        return clientAddress;
    }

    public void setClientAddress(String clientAddress) {
        this.clientAddress = clientAddress;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "Client is" + getFirstName() + "\t" + getLogin();
    }
}

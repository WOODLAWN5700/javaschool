package tables;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Hiberanate class discolate entety of "delivery_method" table in DB
 * Created by pavelpetrov on 28.09.16.
 */

@Entity
@Table(name = "delivery_method")
@AttributeOverride(name = "entityId", column = @Column(name = "delivery_method_id"))
public class DeliveryMethod extends TableEntity {

    /**
     * name of delivery method in "delivery_method" table
     */
    @Column(name = "delivery_method", unique = true)
    private String deliveryMethod;

    /**
     * set of orderses
     */
    @OneToMany(mappedBy = "deliveryMethod")
    private Set<Orders> orderses = new HashSet<Orders>();

    public Set<Orders> getOrderses() {
        return orderses;
    }

    public void setOrderses(Set<Orders> orderses) {
        this.orderses = orderses;
    }

    public String getDeliveryMethod() {
        return deliveryMethod;
    }

    public void setDeliveryMethod(String deliveryMethod) {
        this.deliveryMethod = deliveryMethod;
    }
}

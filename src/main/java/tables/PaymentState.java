package tables;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Hiberanate class discolate entity of "payment_state" table in DB
 * Created by pavelpetrov on 28.09.16.
 */

@Entity
@Table(name = "payment_state")
@AttributeOverride(name = "entityId", column = @Column(name = "payment_statement_id"))
public class PaymentState extends TableEntity {

    /**
     * name of payment state in table "payment_state"
     */
    @Column(name = "payment_statement", unique = true)
    private String paymentState;

    /**
     * set of orderses
     */
    @OneToMany(mappedBy = "paymentState")
    private Set<Orders> orderses = new HashSet<Orders>();

    public String getPaymentState() {
        return paymentState;
    }

    public void setPaymentState(String paymentState) {
        this.paymentState = paymentState;
    }
}

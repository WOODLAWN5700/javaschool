package tables;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Hiberanate class discolate entity of "payment_method" table in DB
 * Created by pavelpetrov on 28.09.16.
 */
@Entity
@Table(name = "payment_method")
@AttributeOverride(name = "entityId", column = @Column(name = "payment_method_id"))
public class PaymentMethod extends TableEntity {

    /**
     * name of payment method in table "payment_method"
     */
    @Column(name = "payment_method", unique = true)
    private String paymetnMethod;

    /**
     * set of orderses
     */
    @OneToMany(mappedBy = "paymentMethod")
    private Set<Orders> orderses = new HashSet<Orders>();

    public String getPaymetnMethod() {
        return paymetnMethod;
    }

    public void setPaymetnMethod(String paymetnMethod) {
        this.paymetnMethod = paymetnMethod;
    }
}

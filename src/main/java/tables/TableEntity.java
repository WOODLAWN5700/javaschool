package tables;




import javax.persistence.*;
import java.io.Serializable;

/** Abstract table entity class
 * Created by pavelpetrov on 30.09.16.
 */

@MappedSuperclass
public abstract class TableEntity implements Serializable {

    /**
     * id of abstract class
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int entityId;

    public int getEntityId() {
        return entityId;
    }

    public void setEntityId(int projectTableEntatyId) {
        this.entityId = projectTableEntatyId;
    }
}

package tables;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Hiberanate class discolate entity of "category" table in DB
 * Created by pavelpetrov on 28.09.16.
 */
@Entity
@Table(name = "category")
@AttributeOverride(name = "entityId", column = @Column(name = "category_id"))
public class Category extends TableEntity {

    /**
     * id of previous category
     * for example: doors -> wooden door
     */
    @Column(name = "category")
    private String category;


    /**
     * set of products
     */
    @OneToMany(mappedBy = "category")
    private Set<Product> products = new HashSet<Product>();

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Set<Product> getProducts() {
        return products;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }
}

package tables;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Hiberanate class discolate entity of "order state" table in DB
 * Created by pavelpetrov on 28.09.16.
 */

@Entity
@Table(name = "order_state")
@AttributeOverride(name = "entityId", column = @Column(name = "order_statement_id"))
public class OrderState extends TableEntity {

    /**
     * name of order state
     */
    @Column(name = "order_statement", unique = true)
    private String orderState;

    /**
     * set of orderses
     */
    @OneToMany(mappedBy = "orderState")
    private Set<Orders> orderses = new HashSet<Orders>();

    public String getOrderState() {
        return orderState;
    }

    public void setOrderState(String orderState) {
        this.orderState = orderState;
    }

    public Set<Orders> getOrderses() {
        return orderses;
    }

    public void setOrderses(Set<Orders> orderses) {
        this.orderses = orderses;
    }
}

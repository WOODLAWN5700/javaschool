package tables;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Hiberanate class discolate entity of "orders" table in DB
 * Created by pavelpetrov on 27.09.16.
 */
@Entity
@Table(name = "orders")
@AttributeOverride(name = "entityId", column = @Column(name = "order_id"))
public class Orders extends TableEntity {

    /**
     * Client's id
     */
    @ManyToOne(
    fetch = FetchType.LAZY)
    @NotFound(
            action = NotFoundAction.IGNORE)
    @JoinColumn(name = "client_id")
    private Client client;

    /**
     * Payment method id
     */
    @ManyToOne(
            fetch = FetchType.LAZY)
    @NotFound(
            action = NotFoundAction.IGNORE)
    @JoinColumn(name = "payment_method_id")
    private PaymentMethod paymentMethod;

    /**
     * many to many relation to product table
     */
    @ManyToMany(
            fetch = FetchType.LAZY)
    @NotFound(
            action = NotFoundAction.IGNORE)
    @JoinTable(name = "order_product", joinColumns = @JoinColumn(name = "order_id"),
            inverseJoinColumns = @JoinColumn(name = "product_id"))
    @Column(name = "product_id")
    private Set<Product> products = new HashSet<Product>();

    /**
     * payment state id
     */
    @ManyToOne(
            fetch = FetchType.LAZY)
    @NotFound(
            action = NotFoundAction.IGNORE)
    @JoinColumn(name = "payment_state_id")
    private PaymentState paymentState;

    /**
     * order state id
     */
    @ManyToOne(
            fetch = FetchType.LAZY)
    @NotFound(
            action = NotFoundAction.IGNORE)
    @JoinColumn(name = "order_statement_id")
    private OrderState orderState;

    /**
     * delivery method id
     */
    @ManyToOne(
            fetch = FetchType.LAZY)
    @NotFound(
            action = NotFoundAction.IGNORE)
    @JoinColumn(name = "delivery_method_id")
    private DeliveryMethod deliveryMethod;

    /**
     * date of order
     */
    @Temporal(TemporalType.DATE)
    @Column(name = "date_of_order")
    private Date dateOfOrder;

    /**
     * comments
     */
    @Column(name = "comments")
    private String comments;

    public Date getDateOfOrder() {
        return dateOfOrder;
    }

    public void setDateOfOrder(Date dateOfOrder) {
        this.dateOfOrder = dateOfOrder;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public OrderState getOrderState() {
        return orderState;
    }

    public void setOrderState(OrderState orderState) {
        this.orderState = orderState;
    }

    public DeliveryMethod getDeliveryMethod() {
        return deliveryMethod;
    }

    public void setDeliveryMethod(DeliveryMethod deliveryMethod) {
        this.deliveryMethod = deliveryMethod;
    }

    public PaymentState getPaymentState() {
        return paymentState;
    }

    public void setPaymentState(PaymentState paymentState) {
        this.paymentState = paymentState;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public Set<Product> getProducts() {
        return products;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }
}

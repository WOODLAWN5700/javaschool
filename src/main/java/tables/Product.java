package tables;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Hiberanate class discolate entity of "product" table in DB
 * Created by pavelpetrov on 28.09.16.
 */

@Entity
@Table(name = "product")
@AttributeOverride(name = "entityId", column = @Column(name = "product_id"))
public class Product extends TableEntity {


    /**
     * id of category
     */
    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

    /**
     * description of product
     */
    @Column(name = "description")
    private String description;

    /**
     * price of product
     */
    @Column(name = "price")
    private int price;

    /**
     * name of Product
     */
    @Column(name = "product_name")
    private String productName;

    /**
     * oaramet id
     */
    @ManyToOne
    @JoinColumn(name = "parametr_id")
    private Params parametr;

    /**
     * avalible quantity of product
     */
    @Column(name = "quantity")
    private int quantity;

    /**
     * many to many relation to order table
     */
    @ManyToMany(mappedBy = "products")
    private Set<Orders> orderses = new HashSet<Orders>();

    /**
     * private count of products on cart
     */
    @Transient
    private int quantityOfProductsOnCart;


    /**
     * Product table constructor
     *
     * @param description
     * @param price
     * @param productName
     * @param quantity
     */
    public Product(String description, int price, String productName, int quantity) {
        this.description = description;
        this.price = price;
        this.productName = productName;
        this.quantity = quantity;
        this.orderses = orderses;
    }

    public Product() {
    }

    public int getQuantityOfProductsOnCart() {
        return quantityOfProductsOnCart;
    }

    public void setQuantityOfProductsOnCart(int quantityOfProductsOnCart) {
        this.quantityOfProductsOnCart = quantityOfProductsOnCart;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Set<Orders> getOrderses() {
        return orderses;
    }

    public void setOrderses(Set<Orders> orderses) {
        this.orderses = orderses;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Params getParametr() {
        return parametr;
    }

    public void setParametr(Params parametr) {
        this.parametr = parametr;
    }

    /**
     * override equals method
     * @param =
     * @return boolean
     */
    @Override
    public boolean equals(Object obj) {
        Product obj2 = (Product) obj;
        if (this.getDescription().equals(obj2.getDescription())
                && this.getProductName().equals(obj2.getProductName())
                && this.getPrice() == obj2.getPrice()) {
            return true;
        } else {
            return false;
        }
    }
}

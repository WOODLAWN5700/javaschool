package tables;

import javax.persistence.*;

/**
 * Hiberanate class discolate entity of "product_param" table in DB
 * Created by pavelpetrov on 28.09.16.
 */

@Entity
@Table(name = "product_param")
public class ProductParam {


    /**
     * id of product_param
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int productParamId;

    /**
     * foreign key of product id
     */
    @Column(name = "product_id_fk")
    private int productIdFk;

    /**
     * foreign key of param id
     */
    @Column(name = "param_id_fk")
    private int paramIdFk;

    public int getProductParamId() {
        return productParamId;
    }

    public void setProductParamId(int productParamId) {
        this.productParamId = productParamId;
    }

    public int getProductIdFk() {
        return productIdFk;
    }

    public void setProductIdFk(int productIdFk) {
        this.productIdFk = productIdFk;
    }

    public int getParamIdFk() {
        return paramIdFk;
    }

    public void setParamIdFk(int paramIdFk) {
        this.paramIdFk = paramIdFk;
    }
}

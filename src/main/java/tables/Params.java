package tables;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Hiberanate class discolate entity of "params" table in DB
 * Created by pavelpetrov on 28.09.16.
 */

@Entity
@Table(name = "params")
@AttributeOverride(name = "entityId", column = @Column(name = "param_id"))
public class Params extends TableEntity {


    /**
     * brand id in "params" table
     */
    @Column(name = "brand")
    private String brand;

    /**
     * power id in "params" table
     */
    @Column(name = "power")
    private int power;

    /**
     * color id in "params" table
     */
    @Column(name = "color")
    private String color;

    /**
     * weight id in "params" table
     */
    @Column(name = "weight")
    private int weigth;

    /**
     * set of products
     */
    @OneToMany(mappedBy = "parametr")
    private Set<Product> products = new HashSet<Product>();

    /**
     * constrctor with params
     * @param brand
     * @param color
     * @param power
     * @param weigth
     */
    public Params(String brand, String color, int power, int weigth) {
        this.brand = brand;
        this.color = color;
        this.power = power;
        this.weigth = weigth;
    }

    /**
     * constructor without params
     */
    public Params() {
    }

    public String getBrand() {
        return brand;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public String getColor() {
        return color;
    }

    public int getWeigth() {
        return weigth;
    }

    public void setWeigth(int weigthId) {
        this.weigth = weigth;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Set<Product> getProducts() {
        return products;
    }

    public void setProducts(Set<Product> products) {
        this.products = products;
    }
}

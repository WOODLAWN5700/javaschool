/**
 * Created by pavelpetrov on 18.10.16.
 */
var category;
$(function () {
    category = $("#createNewCategoryInput");
    $("#createNewCategoryBtn").click(function(event) {
        clientRegistration(event);
    });
});

function clientRegistration (event) {
    event.preventDefault();
    $.ajax({
        method: "POST",
        url: "/CreateNewCategoryServlet",
        data: {
            category: category.val()
          
        },
        success: function (result) {
            if (!result.success) {
                alert(result.message);
            } else {
                alert("New Category have been added");
            }
        },
        error:function (a,b,c) {
            alert(a.responseText);
        }
    })
}
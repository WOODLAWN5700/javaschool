<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: pavelpetrov
  Date: 15.10.16
  Time: 23:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
	<meta charset="UTF-8">
	<title>Header</title>
	<link rel="stylesheet"
		  href="${pageContext.request.contextPath}//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/HTMLCSS/style.css">
</head>
<body>

<nav class="navbar navbar-inverse">
	<div class="container-fluid">
		<div class="navbar-header">
			<a class="navbar-brand" href="#">E-Shop Google</a>
		</div>
		<ul class="nav navbar-nav">
			<li class="active"><a href="http://localhost:8080/ProductListServlet">Home</a></li>
			<c:if test="${sessionScope.get('username') eq 'Admin'}">
				<li><a href="${pageContext.request.contextPath}/OrderCreatorListAdmin">Order List Admin</a></li>
				<li><a href="http://localhost:8080/CreateCategoryList">Product
					registration</a>
				</li><li><a href="${pageContext.request.contextPath}/HTMLCSS/CreateNewCategoryAdmin.jsp">Crate new Category</a>
				</li>
			</c:if>
		</ul>
		<ul class="nav navbar-nav navbar-right">

			<li>
				<a href="${pageContext.request.contextPath}/HTMLCSS/MyCard.jsp">
					<span class="glyphicon glyphicon-shopping-cart shopping-cart-image">
						<c:if test="${ProductsOnCardQuantity ne null}">
						<span class="badge">
							<c:out value="${ProductsOnCardQuantity}"/>
						</span>
						</c:if>
					</span>
				</a>
			</li>

			<li><a href="${pageContext.request.contextPath}/HTMLCSS/MyCard.jsp"><c:out value="${TotalPrice}"/></a></li>
			<c:choose>
				<c:when test="${sessionScope.get('username') ne null}">
					<li><a href="${pageContext.request.contextPath}/HTMLCSS/ProfileForm.jsp"><span
							class="glyphicon glyphicon-user"></span>My profile</a></li>
					<li><a href="${pageContext.request.contextPath}/LogOutSetrvlet"><span
							class="glyphicon glyphicon-log-out"></span>Log out</a></li>
				</c:when>
				<c:when test="${sessionScope.get('username') eq null}">
					<li><a href="/index.jsp"><span class="glyphicon glyphicon-log-in"></span>Login</a></li>
				</c:when>
			</c:choose>
		</ul>
	</div>

</nav>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="//netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

</body>
</html>
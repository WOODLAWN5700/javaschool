<%@ page import="tables.Product" %>
<%@ page import="java.util.List" %>
<%@ page import="tables.Params" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>ProductList</title>
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
	<link rel="stylesheet" href="/HTMLCSS/style.css">
</head>
<body>

<c:import url="Header.jsp"/>

<div class="container">

		<div class="row">
			<button data-toggle="collapse" data-target="#demo" class="btn btn-primary btn-block">Make a Sort</button>
			<div id="demo" class="collapse">
			<div class="col-xs-12">
				<h4>Sort by:</h4>
				<a href="javascript: void(0)" class="sort-predicate btn btn-info"
				   data-predicate="productName">product name</a>
				<a href="javascript: void(0)" class="sort-predicate btn btn-info" data-predicate="price">price</a>
				<a href="javascript: void(0)" class="sort-predicate btn btn-info" data-predicate="brand">brand</a>
				<a href="javascript: void(0)" class="sort-predicate btn btn-info" data-predicate="category">category</a>
			</div>
		</div>
			<br>
			<button data-toggle="collapse" data-target="#filterDiv" class="btn btn-primary btn-block">Make a Filter</button>
			<div id="filterDiv" class="collapse">
		<h4>Filter by:</h4>
		<div class="row">
			<div class="divText1">
				<select class="form-control" id="categoryFilter2" name="categoryFilter">
					<option var="defoult" value="">Choose category</option>
					<c:forEach var="categoryListFilter" items="${UniqueCategoryList}" varStatus="categoryListCount">
						<option value="${categoryListFilter.category}">${categoryListFilter.category}</option>
					</c:forEach>
				</select>
			</div>
			<div class="divText1">
				<select class="form-control" id="brandFilter" name="brandFilter">
					<option var="defoult" value="">Choose brad</option>
					<c:forEach var="brandFilter" items="${UniqueBrandList}" varStatus="categoryListCount">
						<option value="${brandFilter}">${brandFilter}</option>
					</c:forEach>
				</select>
			</div>

			<div class="divText1">
				<select class="form-control" id="colorFilter" name="brandFilter">
					<option var="defoult" value="">Choose color</option>
					<c:forEach var="colorFilter" items="${UniqueColorList}" varStatus="categoryListCount">
						<option value="${colorFilter}">${colorFilter}</option>
					</c:forEach>
				</select>
			</div>

			<div class="divText1">
				<select class="form-control" id="powerFilter" name="brandFilter">
					<option var="defoult" value="">Choose power</option>
					<c:forEach var="powerFilter" items="${UniquePowerList}" varStatus="categoryListCount">
						<option value="${powerFilter}">${powerFilter}</option>
					</c:forEach>
				</select>
			</div>

			<div class="divText1">
				<select class="form-control" id="weightFilter" name="brandFilter">
					<option var="defoult" value="">Choose weight</option>
					<c:forEach var="weightFilter" items="${UniqueWeightList}" varStatus="categoryListCount">
						<option value="${weightFilter}">${weightFilter}</option>
					</c:forEach>
				</select>
			</div>

			<section class="range-slider" id="sliderFilter">
				<span class="rangeValues"></span>
				<input value="0" min="0" max="5000" step="1" type="range" id="priceMinFilter">
				<input value="5000" min="0" max="5000" step="1" type="range" id="priceMaxFilter">
			</section>
			<button type="button" class="btn btn-primary btn-block" id="doFilterBtn">Do filter</button>
			</div>


		</div>
		<div class="col-xs-12 " id="tableDiv">
			<table class="table table-hover" id="product-list">
				<thead>
				<tr>
					<th class="text-left" colspan="2">Category</th>
					<th class="text-left">Product Name</th>
					<th class="text-left">Price</th>
					<th class="text-left">Brand</th>
					<th class="text-left">Description</th>
				</tr>
				</thead>
				<tbody>
				<c:forEach var="orderAdminList" items="${ProductList}" varStatus="productListCount">

					<tr>
						<form action="${pageContext.request.contextPath}/AddTOCard" method="post">
							<td class="Button_add_to_card">
							<span data-toggle="tooltip"
								  title="Add To Cart">
								<button class="btn btn-default"
										type="submit"
										name="Button"
										value="${productListCount.index}">
									<span class="glyphicon glyphicon-plus"></span>
								</button>
							</span>

							</td>
						</form>


						<!-- Modal -->
						<div class="modal fade" id="modal-product-${productListCount.index}" tabindex="-1" role="dialog"
							 aria-labelledby="myModalLabel">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal"
												aria-label="Close"><span
												aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myModalLabel">Modal title</h4>
									</div>
									<div class="modal-body">

											${orderAdminList.parametr.power}

									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								</div>
							</div>
						</div>

						<td class="text-left">${orderAdminList.category.category}</td>
						<td class="text-left"><a href="javascript: void(0)" data-toggle="modal"
												 data-target="#modal-product-${productListCount.index}">${orderAdminList.productName}</a>
						</td>
						<td class="text-left">${orderAdminList.price}</td>
						<td class="text-left">${orderAdminList.parametr.brand}</td>
						<td class="text-left">${orderAdminList.description}</td>
					</tr>
				</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="//netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/HTMLCSS/ProductListJs.js"></script>

<script>
	var sendFilterRequest = function (predicate) {
		$.ajax({
			method: "POST",
			url: "/SelectServlet",
			data: {
				"sort": predicate
			},
			success: function (response) {
//				window.location.reload();
				$('#tableDiv').load('/HTMLCSS/ProductList.jsp #tableDiv');
			},
			error: function (xhr, status, error) {
				alert(error);
			}
		});
	}

	$(".sort-predicate").click(function (event) {
		$(".sort-predicate").removeClass("active");
		$(this).addClass("active");
		var predicate = $(this).attr("data-predicate");
		sendFilterRequest(predicate);
	});

	$(function () {
		$('[data-toggle="tooltip"]').tooltip()
	})

	//$("#myModal").modal();


</script>

</body>
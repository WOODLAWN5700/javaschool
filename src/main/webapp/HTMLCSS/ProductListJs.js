/**
 * Created by pavelpetrov on 19.10.16.
 */
var categoryFilter;
var brandFilter;
var colorFilter;
var powerFilter;
var weightFilter;
var priceMinFilter;
var priceMaxFilter;

$(function () {

    categoryFilter = $("#categoryFilter2");
    brandFilter = $("#brandFilter");
    colorFilter = $("#colorFilter");
    powerFilter = $("#powerFilter");
    weightFilter = $("#weightFilter");
    priceMinFilter = $("#priceMinFilter");
    priceMaxFilter = $("#priceMaxFilter");
    
    $("#doFilterBtn").click(function(event) {
        doFilterProduct(event);
    });
});

function doFilterProduct (event) {
    event.preventDefault();
    $.ajax({
        method: "POST",
        url: "/FilterProductServlet",
        data: {
            categoryFilter : categoryFilter.val(),
            brandFilter : brandFilter.val(),
            colorFilter : colorFilter.val(),
            powerFilter : powerFilter.val(),
            weightFilter : weightFilter.val(),
            priceMinFilter : priceMinFilter.val(),
            priceMaxFilter : priceMaxFilter.val()
           
        },
        success: function (result) {
            if (!result.success) {
                alert(result.message);
            } else {
                $('#tableDiv').load('/HTMLCSS/ProductList.jsp #tableDiv');
            }
        },
        error:function (a,b,c) {
            alert(a.responseText);
        }
    })
}



function getVals(){
    // Get slider values
    var parent = this.parentNode;
    var slides = parent.getElementsByTagName("input");
    var slide1 = parseFloat( slides[0].value );
    var slide2 = parseFloat( slides[1].value );
    // Neither slider will clip the other, so make sure we determine which is larger
    if( slide1 > slide2 ){ var tmp = slide2; slide2 = slide1; slide1 = tmp; }

    var displayElement = parent.getElementsByClassName("rangeValues")[0];
    displayElement.innerHTML = slide1 + " - " + slide2;
}

window.onload = function(){
    // Initialize Sliders
    var sliderSections = document.getElementsByClassName("range-slider");
    for( var x = 0; x < sliderSections.length; x++ ){
        var sliders = sliderSections[x].getElementsByTagName("input");
        for( var y = 0; y < sliders.length; y++ ){
            if( sliders[y].type ==="range" ){
                sliders[y].oninput = getVals;
                // Manually trigger event first time to display values
                sliders[y].oninput();
            }
        }
    }
};


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Login</title>
	<link rel="stylesheet"
		  href="${pageContext.request.contextPath}//netdna.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css"
		  type="text/css">
	<link rel="stylesheet" href="HTMLCSS/style.css" type="text/css">
</head>
<body>

<nav class="navbar navbar-inverse">
	<div class="container-fluid">
		<div class="navbar-header">
			<a class="navbar-brand" href="#">E-Shop Google</a>
		</div>
		<ul class="nav navbar-nav">
			<li class="active"><a href="http://localhost:8080/ProductListServlet">Home</a></li>
			<c:if test="${sessionScope.get('username') eq 'Admin'}">
				<li><a href="${pageContext.request.contextPath}/OrderCreatorListAdmin">Order List Admin</a></li>
				<li><a href="${pageContext.request.contextPath}/HTMLCSS/ProductRegitrationAdmin.jsp">Product
					registration</a>
				</li>
			</c:if>
		</ul>
		<ul class="nav navbar-nav navbar-right">

			<li>
				<a href="${pageContext.request.contextPath}/HTMLCSS/MyCard.jsp">
					<span class="glyphicon glyphicon-shopping-cart shopping-cart-image">
						<c:if test="${ProductsOnCardQuantity ne null}">
						<span class="badge">
							<c:out value="${ProductsOnCardQuantity}"/>
						</span>
						</c:if>
					</span>
				</a>
			</li>

			<c:choose>
				<c:when test="${sessionScope.get('username') ne null}">
					<li><a href="${pageContext.request.contextPath}/HTMLCSS/ProfileForm.jsp"><span
							class="glyphicon glyphicon-user"></span>My profile</a></li>
					<li><a href="${pageContext.request.contextPath}/LogOutSetrvlet"><span
							class="glyphicon glyphicon-log-out"></span>Log out</a></li>
				</c:when>
				<c:when test="${sessionScope.get('username') eq null}">
					<li><a href="/index.jsp"><span class="glyphicon glyphicon-log-in"></span>Login</a></li>
				</c:when>
			</c:choose>
		</ul>
	</div>
</nav>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<table cellspacing="0" align="center">
	<tr style="margin: 15%">
		<td>
			<form action="" method="post" id="sd">
				<div class="form-element">
					<label for="username">Username: </label>
					<input type="text" name="username" id="username"/>
				</div>

				<div class="form-element">
					<label for="password">Password: </label>
					<input type="password" name="password" id="password"/>
				</div>
				<div style="-webkit-flex-flow: row wrap;
    -ms-flex-flow: row wrap;
    flex-flow: row wrap;
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flexbox;
    display: flex;
    -webkit-justify-content: center;
    -moz-justify-content: center;
    -ms-justify-content: center;
    -webkit-box-pack: center;
    -ms-flex-pack: center;
    justify-content: center;">
					<div id="loginButton" class="loginDiv">
						<button type="submit" name="action" value="login" id="LogButt">Login</button>
					</div>
					<div id="registrationButton" class="loginDiv">
						<button onclick="location.href='HTMLCSS/Registration.jsp'; return false; " id="registration">
							Registration
						</button>
					</div>
				</div>
			</form>
		</td>
	</tr>
</table>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="//netdna.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script src="IndexJs.js"></script>
</body>
</html>

/**
 * Created by pavelpetrov on 17.10.16.
 */
var username;
var password;

$(function () {
    username = $("#username");
    password = $("#password");
    

    $("#LogButt").click(function(event) {
        login(event);
    });
});

function login(event) {
    event.preventDefault();
    $.ajax({
        method: "POST",
        url: "/login",
        data: {
            username: username.val(),
            password: password.val()
            
        },
        success: function (result) {
            if (!result.success) {
                alert(result.message);

            } else {
                location.href='/HTMLCSS/ProductList.jsp';
            }
        },
        error:function (a,b,c) {
            alert(a.responseText);
        }
    })
}